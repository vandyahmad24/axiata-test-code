package main

import (
	"database/sql"
	"fmt"
	"log"
	"os"

	"axiata-test-code-vandy/config"

	_ "github.com/go-sql-driver/mysql"
	migrate "github.com/rubenv/sql-migrate"
)

func main() {
	if len(os.Args) < 2 {
		fmt.Println("Missing parameter, provide action!")
		return
	}

	log.Println(os.Args)

	cfg := config.NewConfig()

	migrateStatus := os.Args[1]

	if migrateStatus == "" {
		fmt.Println("Missing parameter, provide action!")
		return
	}
	username := cfg.MYSQL_USERNAME
	password := cfg.MYSQL_PASSWORD
	host := cfg.MYSQL_HOST
	port := cfg.MYSQL_PORT
	dbname := cfg.MYSQL_DATABASE

	dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8mb4&parseTime=True&loc=Local", username, password, host, port, dbname)
	db, err := sql.Open("mysql", dsn)
	if err != nil {
		panic(err)
	}
	defer db.Close()

	switch migrateStatus {
	case "up":
		applyUpMigrations(db)
	case "down":
		applyDownMigrations(db)
	case "status":
		showMigrationStatus(db)
	default:
		fmt.Println("Invalid action!")
	}
}

func applyUpMigrations(db *sql.DB) {
	log.Println("running up migration")
	migrations := &migrate.FileMigrationSource{
		Dir: "migration",
	}
	m, err := migrate.Exec(db, "mysql", migrations, migrate.Up)
	if err != nil {
		panic(err)
	}
	fmt.Printf("Applied %d migrations!\n", m)
}

func applyDownMigrations(db *sql.DB) {
	log.Println("running down migration")
	migrations := &migrate.FileMigrationSource{
		Dir: "migration",
	}
	m, err := migrate.ExecMax(db, "mysql", migrations, migrate.Down, 1)
	if err != nil {
		panic(err)
	}
	fmt.Printf("Applied %d migrations!\n", m)
}

func showMigrationStatus(db *sql.DB) {
	log.Println("running status migration")
	m, err := migrate.GetMigrationRecords(db, "mysql")
	if err != nil {
		panic(err)
	}
	for _, record := range m {
		fmt.Printf("%s - %s\n", record.Id, record.AppliedAt)
	}
}
