package main

import (
	"fmt"
	"log"

	"github.com/gofiber/fiber/v2"

	"axiata-test-code-vandy/config"
	"axiata-test-code-vandy/internal/handler"
	"axiata-test-code-vandy/internal/repository"
	"axiata-test-code-vandy/internal/usecase"
	"axiata-test-code-vandy/lib/logger"
)

func main() {
	logger.Init()

	db, err := config.NewMySQL()
	if err != nil {
		panic(err)
	}

	cfg := config.NewConfig()

	postRepo := repository.NewPostRepository(db)
	postUsecase := usecase.NewPostUsecase(postRepo)
	postHandler := handler.NewPostHandler(postUsecase)

	app := fiber.New()

	app.Post("/api/posts", postHandler.CreatePost)
	app.Get("/api/posts/:id", postHandler.GetPost)
	app.Put("/api/posts/:id", postHandler.UpdatePost)
	app.Delete("/api/posts/:id", postHandler.DeletePost)

	log.Fatal(app.Listen(fmt.Sprintf(":%s", cfg.PORT)))
}
