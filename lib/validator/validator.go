package validator

import (
	"fmt"
	"github.com/go-playground/validator/v10"
)

func ValidateStruct(data interface{}) error {
	validate := validator.New()

	err := validate.Struct(data)
	if err != nil {
		if _, ok := err.(*validator.InvalidValidationError); ok {
			return fmt.Errorf("Invalid validation: %v", err)
		}

		var fields []string
		for _, err := range err.(validator.ValidationErrors) {
			fieldName := err.Field()
			fields = append(fields, fieldName)
		}
		return fmt.Errorf("Validation failed for fields: %v", fields)
	}

	return nil
}
