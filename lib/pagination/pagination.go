package pagination

import (
	"fmt"

	"github.com/gofiber/fiber/v2"
)

type Pagination struct {
	FirstPage string `json:"first"`
	LastPage  string `json:"last"`
	PrevPage  string `json:"prev"`
	NextPage  string `json:"next"`
}

type Meta struct {
	CurrentPage int    `json:"current_page"`
	From        int    `json:"from"`
	LastPage    int    `json:"last_page"`
	Path        string `json:"path"`
	PerPage     int    `json:"per_page"`
	To          int    `json:"to"`
	Total       int64  `json:"total"`
}

func GeneratePaginationLinks(c *fiber.Ctx, page int, pageSize int, total int64) (Pagination, Meta) {
	lastPage := int((total + int64(pageSize) - 1) / int64(pageSize))

	path := c.BaseURL() + c.Path()

	pagination := Pagination{
		FirstPage: fmt.Sprintf("%s?page=1", path),
		LastPage:  fmt.Sprintf("%s?page=%d", path, lastPage),
		PrevPage:  "",
		NextPage:  "",
	}

	if page > 1 {
		pagination.PrevPage = fmt.Sprintf("%s?page=%d", path, page-1)
	}

	if page < lastPage {
		pagination.NextPage = fmt.Sprintf("%s?page=%d", path, page+1)
	}

	meta := Meta{
		CurrentPage: page,
		From:        (page-1)*pageSize + 1,
		LastPage:    lastPage,
		Path:        path,
		PerPage:     pageSize,
		To:          page * pageSize,
		Total:       total,
	}

	return pagination, meta
}
