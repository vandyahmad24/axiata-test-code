dep:
	go mod download

run-api:
	go run cmd/main.go


migrate-up:
	go run cmd/migration/main.go up

migrate-down:
	go run cmd/migration/main.go down

migrate-status:
	go run cmd/migration/main.go status

