package handler

import (
	"axiata-test-code-vandy/internal/request"
	"axiata-test-code-vandy/lib/validator"
	"strconv"
	"time"

	"github.com/gofiber/fiber/v2"

	"axiata-test-code-vandy/internal/model"
	"axiata-test-code-vandy/internal/usecase"
	"axiata-test-code-vandy/lib/logger"
)

type PostHandler struct {
	postUsecase usecase.PostUsecase
}

func NewPostHandler(pu usecase.PostUsecase) *PostHandler {
	return &PostHandler{pu}
}

func (h *PostHandler) CreatePost(c *fiber.Ctx) error {
	ctx := c.Context()
	var post request.PostRequest
	if err := c.BodyParser(&post); err != nil {
		logger.Error(ctx, "Failed to parse request body", map[string]interface{}{"error": err.Error()})
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"status":  "error",
			"message": err.Error(),
		})
	}

	if err := validator.ValidateStruct(post); err != nil {
		logger.Error(ctx, "Failed to validate post", map[string]interface{}{"error": err.Error()})
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"status":  "error",
			"message": err.Error(),
		})
	}

	if post.PublishDate == "" {
		post.PublishDate = time.Now().Format("2006-01-02 15:04:05")
	}
	if post.Status == "" {
		post.Status = "draft"
	}

	if err := h.postUsecase.CreatePost(ctx, &model.Post{
		Title:       post.Title,
		Content:     post.Content,
		Tags:        post.Tags,
		Status:      post.Status,
		PublishDate: post.PublishDate,
	}); err != nil {
		logger.Error(ctx, "Failed to create post", map[string]interface{}{"error": err.Error()})
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"status":  "error",
			"message": err.Error(),
		})
	}

	logger.Info(ctx, "Post created successfully", map[string]interface{}{"post": post})
	return c.Status(fiber.StatusCreated).JSON(fiber.Map{
		"status":  "success",
		"message": "Post created successfully",
		"data":    post,
	})
}

func (h *PostHandler) GetPost(c *fiber.Ctx) error {
	ctx := c.Context()
	id, err := strconv.Atoi(c.Params("id"))
	if err != nil {
		logger.Error(ctx, "Invalid post ID", map[string]interface{}{"error": err.Error()})
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"status":  "error",
			"message": err.Error(),
		})
	}

	post, err := h.postUsecase.GetPostByID(ctx, id)
	if err != nil {
		logger.Error(ctx, "Failed to get post", map[string]interface{}{"error": err.Error()})
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"status":  "error",
			"message": err.Error(),
		})
	}

	logger.Info(ctx, "Get data successfully", map[string]interface{}{"post": post})
	return c.JSON(fiber.Map{
		"status":  "success",
		"message": "Post retrieved successfully",
		"data":    post,
	})
}

func (h *PostHandler) UpdatePost(c *fiber.Ctx) error {
	ctx := c.Context()
	id, err := strconv.Atoi(c.Params("id"))
	if err != nil {
		logger.Error(ctx, "Invalid post ID", map[string]interface{}{"error": err.Error()})
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"status":  "error",
			"message": err.Error(),
		})
	}

	var post model.Post
	if err := c.BodyParser(&post); err != nil {
		logger.Error(ctx, "Failed to parse request body", map[string]interface{}{"error": err.Error()})
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"status":  "error",
			"message": err.Error(),
		})
	}
	post.ID = id

	if err := h.postUsecase.UpdatePost(ctx, &post); err != nil {
		logger.Error(ctx, "Failed to update post", map[string]interface{}{"error": err.Error()})
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"status":  "error",
			"message": err.Error(),
		})
	}

	logger.Info(ctx, "Post updated successfully", map[string]interface{}{"post": post})
	return c.JSON(fiber.Map{
		"status":  "success",
		"message": "Post updated successfully",
		"data":    post,
	})
}

func (h *PostHandler) DeletePost(c *fiber.Ctx) error {
	ctx := c.Context()
	id, err := strconv.Atoi(c.Params("id"))
	if err != nil {
		logger.Error(ctx, "Invalid post ID", map[string]interface{}{"error": err.Error()})
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"status":  "error",
			"message": err.Error(),
		})
	}

	if err := h.postUsecase.DeletePost(ctx, id); err != nil {
		logger.Error(ctx, "Failed to delete post", map[string]interface{}{"error": err.Error()})
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"status":  "error",
			"message": err.Error(),
		})
	}

	logger.Info(ctx, "Post deleted successfully", map[string]interface{}{"post_id": id})
	return c.Status(fiber.StatusOK).JSON(fiber.Map{
		"status":  "success",
		"message": "Post deleted successfully",
	})
}
