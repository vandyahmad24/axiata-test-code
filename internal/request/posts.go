package request

type PostRequest struct {
	Title       string   `json:"title" validate:"required"`
	Content     string   `json:"content" validate:"required"`
	Tags        []string `json:"tags"`
	Status      string   `json:"status,omitempty" validate:"omitempty,oneof=draft publish"`
	PublishDate string   `json:"publish_date"`
}
