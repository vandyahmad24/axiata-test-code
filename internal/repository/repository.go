package repository

import (
	"axiata-test-code-vandy/internal/model"
	"axiata-test-code-vandy/lib/logger"
	"context"
	"database/sql"
	"errors"
)

type PostRepository interface {
	Create(ctx context.Context, post *model.Post) error
	GetByID(ctx context.Context, id int) (*model.Post, error)
	Update(ctx context.Context, post *model.Post) error
	Delete(ctx context.Context, id int) error
}

type postRepository struct {
	db *sql.DB
}

func NewPostRepository(db *sql.DB) PostRepository {
	return &postRepository{db}
}

func (r *postRepository) Create(ctx context.Context, post *model.Post) error {
	tx, err := r.db.BeginTx(ctx, nil)
	if err != nil {
		logger.Error(ctx, "Failed to begin transaction", map[string]interface{}{"error": err.Error()})
		return err
	}

	_, err = tx.ExecContext(
		ctx,
		`INSERT INTO posts (title, content, status, publish_date) VALUES (?, ?, ?, ?)`,
		post.Title, post.Content, post.Status, post.PublishDate,
	)
	if err != nil {
		tx.Rollback()
		logger.Error(ctx, "Failed to insert post", map[string]interface{}{"error": err.Error()})
		return err
	}

	err = tx.QueryRowContext(ctx, `SELECT LAST_INSERT_ID()`).Scan(&post.ID)
	if err != nil {
		tx.Rollback()
		logger.Error(ctx, "Failed to retrieve last insert id", map[string]interface{}{"error": err.Error()})
		return err
	}

	tagMap := make(map[string]int, len(post.Tags))

	if len(post.Tags) > 0 {
		var queryPersiapan string
		var args []interface{}
		for _, tag := range post.Tags {
			queryPersiapan += "?,"
			args = append(args, tag)
		}
		queryPersiapan = queryPersiapan[:len(queryPersiapan)-1]

		query := `SELECT label, id FROM tags WHERE label IN (` + queryPersiapan + `)`

		rows, err := tx.QueryContext(ctx, query, args...)
		if err != nil {
			tx.Rollback()
			logger.Error(ctx, "Failed to get existing tags", map[string]interface{}{"error": err.Error()})
			return err
		}
		defer rows.Close()

		for rows.Next() {
			var tagLabel string
			var tagID int
			if err := rows.Scan(&tagLabel, &tagID); err != nil {
				tx.Rollback()
				logger.Error(ctx, "Failed to scan tag", map[string]interface{}{"error": err.Error()})
				return err
			}
			tagMap[tagLabel] = tagID
		}
	}

	for _, tagLabel := range post.Tags {
		tagID, ok := tagMap[tagLabel]
		if !ok {
			_, err = tx.ExecContext(ctx, `INSERT INTO tags (label) VALUES (?)`, tagLabel)
			if err != nil {
				tx.Rollback()
				logger.Error(ctx, "Failed to insert tag", map[string]interface{}{"error": err.Error()})
				return err
			}

			err = tx.QueryRowContext(ctx, `SELECT LAST_INSERT_ID()`).Scan(&tagID)
			if err != nil {
				tx.Rollback()
				logger.Error(ctx, "Failed to retrieve last insert id for tag", map[string]interface{}{"error": err.Error()})
				return err
			}
		}

		_, err = tx.ExecContext(ctx, `INSERT INTO post_tags (post_id, tag_id) VALUES (?, ?)`, post.ID, tagID)
		if err != nil {
			tx.Rollback()
			logger.Error(ctx, "Failed to insert post_tag", map[string]interface{}{"error": err.Error()})
			return err
		}
	}

	err = tx.Commit()
	if err != nil {
		logger.Error(ctx, "Failed to commit transaction", map[string]interface{}{"error": err.Error()})
		return err
	}

	logger.Info(ctx, "Post created in repository successfully", map[string]interface{}{"post": post})
	return nil
}

func (r *postRepository) GetByID(ctx context.Context, id int) (*model.Post, error) {
	var post model.Post
	err := r.db.QueryRowContext(ctx, `SELECT id, title, content, status, publish_date FROM posts WHERE id=?`, id).Scan(&post.ID, &post.Title, &post.Content, &post.Status, &post.PublishDate)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Error(ctx, "Post not found", map[string]interface{}{"id": id})
			return nil, errors.New("Failed to get post by ID")
		}
		logger.Error(ctx, "Failed to get post by ID", map[string]interface{}{"error": err.Error()})
		return nil, err
	}

	rows, err := r.db.QueryContext(ctx, `SELECT t.label FROM tags t JOIN post_tags pt ON t.id = pt.tag_id WHERE pt.post_id = ?`, id)
	if err != nil {
		logger.Error(ctx, "Failed to get tags for post", map[string]interface{}{"error": err.Error()})
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var tag string
		if err := rows.Scan(&tag); err != nil {
			logger.Error(ctx, "Failed to scan tag", map[string]interface{}{"error": err.Error()})
			return nil, err
		}
		post.Tags = append(post.Tags, tag)
	}

	logger.Info(ctx, "Post retrieved from repository successfully", map[string]interface{}{"post": post})
	return &post, nil
}

func (r *postRepository) Update(ctx context.Context, post *model.Post) error {
	tx, err := r.db.BeginTx(ctx, nil)
	if err != nil {
		logger.Error(ctx, "Failed to begin transaction", map[string]interface{}{"error": err.Error()})
		return err
	}

	// Delete existing post_tags
	_, err = tx.ExecContext(ctx, `DELETE FROM post_tags WHERE post_id=?`, post.ID)
	if err != nil {
		tx.Rollback()
		logger.Error(ctx, "Failed to delete existing tags", map[string]interface{}{"id": post.ID, "error": err.Error()})
		return err
	}

	tagMap := make(map[string]int, len(post.Tags))

	if len(post.Tags) > 0 {
		var queryPersiapan string
		var args []interface{}
		for _, tag := range post.Tags {
			queryPersiapan += "?,"
			args = append(args, tag)
		}
		queryPersiapan = queryPersiapan[:len(queryPersiapan)-1]

		query := `SELECT label, id FROM tags WHERE label IN (` + queryPersiapan + `)`

		rows, err := tx.QueryContext(ctx, query, args...)
		if err != nil {
			tx.Rollback()
			logger.Error(ctx, "Failed to get existing tags", map[string]interface{}{"error": err.Error()})
			return err
		}
		defer rows.Close()

		for rows.Next() {
			var tagLabel string
			var tagID int
			if err := rows.Scan(&tagLabel, &tagID); err != nil {
				tx.Rollback()
				logger.Error(ctx, "Failed to scan tag", map[string]interface{}{"error": err.Error()})
				return err
			}
			tagMap[tagLabel] = tagID
		}
	}

	for _, tagLabel := range post.Tags {
		tagID, ok := tagMap[tagLabel]
		if !ok {
			_, err = tx.ExecContext(ctx, `INSERT INTO tags (label) VALUES (?)`, tagLabel)
			if err != nil {
				tx.Rollback()
				logger.Error(ctx, "Failed to insert tag", map[string]interface{}{"error": err.Error()})
				return err
			}

			// Retrieve the last inserted id for the tag
			err = tx.QueryRowContext(ctx, `SELECT LAST_INSERT_ID()`).Scan(&tagID)
			if err != nil {
				tx.Rollback()
				logger.Error(ctx, "Failed to retrieve last insert id for tag", map[string]interface{}{"error": err.Error()})
				return err
			}
		}

		_, err = tx.ExecContext(ctx, `INSERT INTO post_tags (post_id, tag_id) VALUES (?, ?)`, post.ID, tagID)
		if err != nil {
			tx.Rollback()
			logger.Error(ctx, "Failed to insert post_tag", map[string]interface{}{"error": err.Error()})
			return err
		}
	}

	_, err = tx.ExecContext(ctx, `UPDATE posts SET title=?, content=? WHERE id=?`,
		post.Title, post.Content, post.ID)
	if err != nil {
		tx.Rollback()
		logger.Error(ctx, "Failed to update post", map[string]interface{}{"error": err.Error()})
		return err
	}

	err = tx.Commit()
	if err != nil {
		logger.Error(ctx, "Failed to commit transaction", map[string]interface{}{"error": err.Error()})
		return err
	}

	logger.Info(ctx, "Post updated in repository successfully", map[string]interface{}{"post": post})
	return nil
}

func (r *postRepository) Delete(ctx context.Context, id int) error {
	tx, err := r.db.BeginTx(ctx, nil)
	if err != nil {
		logger.Error(ctx, "Failed to begin transaction", map[string]interface{}{"error": err.Error()})
		return err
	}

	var tagsToDelete []int
	rows, err := tx.QueryContext(ctx, `SELECT tag_id FROM post_tags WHERE post_id=?`, id)
	if err != nil {
		tx.Rollback()
		logger.Error(ctx, "Failed to get post tags", map[string]interface{}{"error": err.Error()})
		return err
	}
	defer rows.Close()

	if len(tagsToDelete) > 0 {
		_, err = tx.ExecContext(ctx, `DELETE FROM tags WHERE id IN (?)`, tagsToDelete)
		if err != nil {
			tx.Rollback()
			logger.Error(ctx, "Failed to delete tags", map[string]interface{}{"error": err.Error()})
			return err
		}
	}

	for rows.Next() {
		var tagID int
		if err := rows.Scan(&tagID); err != nil {
			tx.Rollback()
			logger.Error(ctx, "Failed to scan tag ID", map[string]interface{}{"error": err.Error()})
			return err
		}
		tagsToDelete = append(tagsToDelete, tagID)
	}

	_, err = tx.ExecContext(ctx, `DELETE FROM post_tags WHERE post_id=?`, id)
	if err != nil {
		tx.Rollback()
		logger.Error(ctx, "Failed to delete post_tags", map[string]interface{}{"error": err.Error()})
		return err
	}

	_, err = tx.ExecContext(ctx, `DELETE FROM posts WHERE id=?`, id)
	if err != nil {
		tx.Rollback()
		logger.Error(ctx, "Failed to delete post", map[string]interface{}{"error": err.Error()})
		return err
	}

	err = tx.Commit()
	if err != nil {
		logger.Error(ctx, "Failed to commit transaction", map[string]interface{}{"error": err.Error()})
		return err
	}

	logger.Info(ctx, "Post deleted in repository successfully", map[string]interface{}{"post_id": id})
	return nil
}
