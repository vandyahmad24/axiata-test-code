package usecase

import (
	"axiata-test-code-vandy/internal/model"
	"axiata-test-code-vandy/internal/repository"
	"axiata-test-code-vandy/lib/logger"
	"context"
)

type PostUsecase interface {
	CreatePost(ctx context.Context, post *model.Post) error
	GetPostByID(ctx context.Context, id int) (*model.Post, error)
	UpdatePost(ctx context.Context, post *model.Post) error
	DeletePost(ctx context.Context, id int) error
}

type postUsecase struct {
	postRepo repository.PostRepository
}

func NewPostUsecase(pr repository.PostRepository) PostUsecase {
	return &postUsecase{pr}
}

func (u *postUsecase) CreatePost(ctx context.Context, post *model.Post) error {

	if err := u.postRepo.Create(ctx, post); err != nil {
		logger.Error(ctx, "Failed to create post in repository", map[string]interface{}{"error": err.Error()})
		return err
	}
	logger.Info(ctx, "Post created in repository successfully", map[string]interface{}{"post": post})
	return nil
}

func (u *postUsecase) GetPostByID(ctx context.Context, id int) (*model.Post, error) {
	post, err := u.postRepo.GetByID(ctx, id)
	if err != nil {
		logger.Error(ctx, "Failed to get post from repository", map[string]interface{}{"error": err.Error()})
		return nil, err
	}
	logger.Info(ctx, "Post retrieved from repository successfully", map[string]interface{}{"post": post})
	return post, nil
}

func (u *postUsecase) UpdatePost(ctx context.Context, post *model.Post) error {
	post, err := u.postRepo.GetByID(ctx, post.ID)
	if err != nil {
		logger.Error(ctx, "Failed to get post from repository", map[string]interface{}{"error": err.Error()})
		return err
	}

	if err := u.postRepo.Update(ctx, post); err != nil {
		logger.Error(ctx, "Failed to update post in repository", map[string]interface{}{"error": err.Error()})
		return err
	}
	logger.Info(ctx, "Post updated in repository successfully", map[string]interface{}{"post": post})
	return nil
}

func (u *postUsecase) DeletePost(ctx context.Context, id int) error {

	_, err := u.postRepo.GetByID(ctx, id)
	if err != nil {
		logger.Error(ctx, "Failed to get post from repository", map[string]interface{}{"error": err.Error()})
		return err
	}

	if err := u.postRepo.Delete(ctx, id); err != nil {
		logger.Error(ctx, "Failed to delete post from repository", map[string]interface{}{"error": err.Error()})
		return err
	}
	logger.Info(ctx, "Post deleted from repository successfully", map[string]interface{}{"post_id": id})
	return nil
}
