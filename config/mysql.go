package config

import (
	"axiata-test-code-vandy/lib/logger"
	"context"
	"database/sql"
	"fmt"
	"log"

	_ "github.com/go-sql-driver/mysql"
)

func NewMySQL() (*sql.DB, error) {
	username := EnvConfig.MYSQL_USERNAME
	password := EnvConfig.MYSQL_PASSWORD
	host := EnvConfig.MYSQL_HOST
	port := EnvConfig.MYSQL_PORT
	dbname := EnvConfig.MYSQL_DATABASE

	dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8mb4&parseTime=True&loc=Local", username, password, host, port, dbname)
	db, err := sql.Open("mysql", dsn)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}

	if err = db.Ping(); err != nil {
		log.Fatal(err)
		return nil, err
	}

	logger.Info(context.Background(), "successfully connected to mysql", make(map[string]interface{}))

	return db, nil

}
